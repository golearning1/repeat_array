package main
  
import "fmt"
  
func main() {
  
    
    my_arr1 := [6]int{12, 456, 67, 65, 34, 34}
  
    my_arr2 := &my_arr1
  
    fmt.Println("Array_1: ", my_arr1)
    fmt.Println("Array_2:", my_arr2)
  
    my_arr1[5] = 1000000
	
    fmt.Println("\nArray_1: ", my_arr1)
    fmt.Println("Array_2:", *my_arr2)
}